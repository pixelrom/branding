#!/bin/bash

TEMPLATE=xda_template.txt

function genDevice() {
    DEVICE=$1
    cat ${TEMPLATE} | sed "s+\^DEVICE\^+${DEVICE}+" | sed "s+master+$(git log -n 1 --pretty=oneline | awk '{ print $1 }')+g"> op_${DEVICE}.txt
}

genDevice dipper
genDevice curtana
genDevice polaris
genDevice beryllium

git add -A
git commit -m "branding: Generate template for $(git log -n 1 --pretty=oneline | awk '{ print $1 }')" -s
